/*
 * Copyright (c) 2019, 2020 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <ngx_http_variables.h>

#include <err.h>

#include <arpa2/a2aclr.h>

#define MAXERRSTR 100

#define ACL_GET                       'R'
#define ACL_HEAD                      'P'
#define ACL_POST                      'C'
#define ACL_PUT                       'W'
#define ACL_DELETE                    'D'
#define ACL_PATCH                     'W'

typedef struct {
	void *ctx;
	ngx_str_t dbpath;
} ngx_http_auth_a2aclr_srv_conf_t;

typedef struct {
	ngx_http_complex_value_t *realm;
	ngx_http_complex_value_t *class;
	ngx_http_complex_value_t *instance;
} ngx_http_auth_a2aclr_loc_conf_t;

static ngx_int_t ngx_http_auth_a2aclr_init(ngx_conf_t *cf);
static void *ngx_http_auth_a2aclr_create_srv_conf(ngx_conf_t *);
static void *ngx_http_auth_a2aclr_create_loc_conf(ngx_conf_t *);
static char *ngx_http_auth_a2aclr_merge_srv_conf(ngx_conf_t *, void *, void *);
static char *ngx_http_auth_a2aclr_merge_loc_conf(ngx_conf_t *, void *, void *);

static ngx_command_t ngx_http_auth_a2aclr_commands[] = {
	{
		ngx_string("auth_a2aclr_db"),
		NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_CONF_TAKE1,
		ngx_conf_set_str_slot,
		NGX_HTTP_SRV_CONF_OFFSET,
		offsetof(ngx_http_auth_a2aclr_srv_conf_t, dbpath),
		NULL
	},
	{
		ngx_string("auth_a2aclr_realm"),
		NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF | NGX_HTTP_UPS_CONF | NGX_CONF_TAKE1,
		ngx_http_set_complex_value_slot,
		NGX_HTTP_LOC_CONF_OFFSET,
		offsetof(ngx_http_auth_a2aclr_loc_conf_t, realm),
		NULL
	},
	{
		ngx_string("auth_a2aclr_class"),
		NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF | NGX_HTTP_UPS_CONF | NGX_CONF_TAKE1,
		ngx_http_set_complex_value_slot,
		NGX_HTTP_LOC_CONF_OFFSET,
		offsetof(ngx_http_auth_a2aclr_loc_conf_t, class),
		NULL
	},
	{
		ngx_string("auth_a2aclr_instance"),
		NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF | NGX_HTTP_UPS_CONF | NGX_CONF_TAKE1,
		ngx_http_set_complex_value_slot,
		NGX_HTTP_LOC_CONF_OFFSET,
		offsetof(ngx_http_auth_a2aclr_loc_conf_t, instance),
		NULL
	},
	ngx_null_command
};

static ngx_http_module_t ngx_http_auth_a2aclr_module_ctx = {
	NULL,			/* preconfiguration */
	ngx_http_auth_a2aclr_init,	/* postconfiguration */

	NULL,			/* create main configuration */
	NULL,			/* init main configuration */

	ngx_http_auth_a2aclr_create_srv_conf,
	ngx_http_auth_a2aclr_merge_srv_conf,

	ngx_http_auth_a2aclr_create_loc_conf,
	ngx_http_auth_a2aclr_merge_loc_conf
};

ngx_module_t ngx_http_auth_a2aclr_module = {
	NGX_MODULE_V1,
	&ngx_http_auth_a2aclr_module_ctx, /* module context */
	ngx_http_auth_a2aclr_commands,    /* module directives */
	NGX_HTTP_MODULE,	/* module type */
	NULL,			/* init master */
	NULL,			/* init module */
	NULL,			/* init process */
	NULL,			/* init thread */
	NULL,			/* exit thread */
	NULL,			/* exit process */
	NULL,			/* exit master */
	NGX_MODULE_V1_PADDING
};

static ngx_int_t
get_remote_user(ngx_http_request_t *r, ngx_http_variable_value_t *v,
    uintptr_t data)
{
	ngx_int_t rc;

	rc = ngx_http_auth_basic_user(r);

	if (rc == NGX_DECLINED) {
		v->not_found = 1;
		return NGX_OK;
	}

	if (rc == NGX_ERROR) {
		return NGX_ERROR;
	}

	v->len = r->headers_in.user.len;
	v->valid = 1;
	v->no_cacheable = 0;
	v->not_found = 0;
	v->data = r->headers_in.user.data;

	return NGX_OK;
}

static ngx_int_t
ngx_http_auth_a2aclr_handler(ngx_http_request_t *r)
{
	ngx_http_variable_value_t remote_user;
	ngx_http_auth_a2aclr_srv_conf_t *srvconf;
	ngx_http_auth_a2aclr_loc_conf_t *locconf;
	ngx_str_t realm;
	ngx_str_t class;
	ngx_str_t instance;
	char reqright;

	ngx_memzero(&remote_user, sizeof remote_user);
	if (get_remote_user(r, &remote_user, 0UL) != NGX_OK) {
		return NGX_HTTP_FORBIDDEN;
	}

	if (!remote_user.valid || remote_user.not_found) {
		return NGX_HTTP_FORBIDDEN;
	}

	switch (r->method) {
	case NGX_HTTP_GET:
		reqright = ACL_GET;
		break;
	case NGX_HTTP_HEAD:
		reqright = ACL_HEAD;
		break;
	case NGX_HTTP_POST:
		reqright = ACL_POST;
		break;
	case NGX_HTTP_PUT:
		reqright = ACL_PUT;
		break;
	case NGX_HTTP_DELETE:
		reqright = ACL_DELETE;
		break;
	default:
		return NGX_HTTP_FORBIDDEN;
	}

	srvconf = ngx_http_get_module_srv_conf(r, ngx_http_auth_a2aclr_module);
	locconf = ngx_http_get_module_loc_conf(r, ngx_http_auth_a2aclr_module);

	if (srvconf->ctx == NULL) {
		ngx_log_error(NGX_LOG_EMERG, r->connection->log, 0,
		    "auth_a2acl module is not configured");

		return NGX_HTTP_FORBIDDEN;
	}

	if (locconf->realm == NULL) {
		ngx_log_error(NGX_LOG_EMERG, r->connection->log, 0,
		    "auth_a2acl_realm is missing");

		return NGX_HTTP_FORBIDDEN;
	}

	if (locconf->class == NULL) {
		ngx_log_error(NGX_LOG_EMERG, r->connection->log, 0,
		    "auth_a2acl_class is missing");

		return NGX_HTTP_FORBIDDEN;
	}

	if (locconf->instance == NULL) {
		ngx_log_error(NGX_LOG_EMERG, r->connection->log, 0,
		    "auth_a2acl_instance is missing");

		return NGX_HTTP_FORBIDDEN;
	}

	if (ngx_http_complex_value(r, locconf->realm, &realm) != NGX_OK) {
		return NGX_ERROR;
	}

	if (ngx_http_complex_value(r, locconf->class, &class) != NGX_OK) {
		return NGX_ERROR;
	}

	if (ngx_http_complex_value(r, locconf->instance, &instance) != NGX_OK) {
		return NGX_ERROR;
	}

	ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
	    "looking up %V %V %V %V %c", &srvconf->dbpath, &realm, &class,
	    &instance, reqright);

	if (a2aclr_hasright(srvconf->ctx, reqright, (char *)realm.data,
	    realm.len, (char *)remote_user.data, remote_user.len,
	    (char *)class.data, class.len, (char *)instance.data, instance.len)
	    == 0) {
		ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
		    "%c: FORBIDDEN", reqright);

		return NGX_HTTP_FORBIDDEN;
	}

	ngx_log_error(NGX_LOG_INFO, r->connection->log, 0, "%c: PERMITTED",
	    reqright);

	return NGX_OK;
}

static ngx_int_t
ngx_http_auth_a2aclr_init(ngx_conf_t *cf)
{
	ngx_http_handler_pt *h;
	ngx_http_core_main_conf_t *cmcf;

	cmcf = ngx_http_conf_get_module_main_conf(cf, ngx_http_core_module);

	h = ngx_array_push(&cmcf->phases[NGX_HTTP_ACCESS_PHASE].handlers);
	if (h == NULL) {
		return NGX_ERROR;
	}

	*h = ngx_http_auth_a2aclr_handler;

	return NGX_OK;
}

static void *
ngx_http_auth_a2aclr_create_srv_conf(ngx_conf_t *cf)
{
	ngx_http_auth_a2aclr_srv_conf_t *conf;

	conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_auth_a2aclr_srv_conf_t));
	if (conf == NULL) {
		return NGX_CONF_ERROR;
	}

	conf->ctx = NULL;

	return conf;
}

static void *
ngx_http_auth_a2aclr_create_loc_conf(ngx_conf_t *cf)
{
	ngx_http_auth_a2aclr_loc_conf_t *conf;

	conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_auth_a2aclr_loc_conf_t));
	if (conf == NULL) {
		return NGX_CONF_ERROR;
	}

	conf->realm = NULL;
	conf->class = NULL;
	conf->instance = NULL;

	return conf;
}

static char *
ngx_http_auth_a2aclr_merge_srv_conf(ngx_conf_t *cf, void *parent, void *child)
{
	char errstr[MAXERRSTR];
	ssize_t updated;

	ngx_http_auth_a2aclr_srv_conf_t *prev = parent;
	ngx_http_auth_a2aclr_srv_conf_t *conf = child;

	ngx_conf_merge_str_value(conf->dbpath, prev->dbpath, "");
	ngx_conf_merge_ptr_value(conf->ctx, prev->ctx, NULL);

	if (conf->ctx != NULL || conf->dbpath.len == 0)
		return NGX_CONF_OK;

	conf->ctx = a2aclr_dbopen(NULL);
	if (conf->ctx == NULL) {
		ngx_log_error(NGX_LOG_EMERG, cf->log, 0, "failed to open new "
		    "database context");

		return NGX_CONF_ERROR;
	}

	updated = a2aclr_importpolicyfile(conf->ctx, (char *)conf->dbpath.data,
	    errstr, sizeof(errstr));

	if (updated == -1) {
		ngx_log_error(NGX_LOG_EMERG, cf->log, 0, "failed to import "
		    "policy file: %V %s", &conf->dbpath, errstr);

		return NGX_CONF_ERROR;
	}

	ngx_log_error(NGX_LOG_NOTICE, cf->log, 0, "%V initialized with %d new "
	    "rules", &conf->dbpath, updated);

	return NGX_CONF_OK;
}

static char *
ngx_http_auth_a2aclr_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
	ngx_http_auth_a2aclr_srv_conf_t *srvconf;

	ngx_http_auth_a2aclr_loc_conf_t *prev = parent;
	ngx_http_auth_a2aclr_loc_conf_t *conf = child;

	srvconf = ngx_http_conf_get_module_srv_conf(cf, ngx_http_auth_a2aclr_module);

	if (srvconf->ctx == NULL) {
		ngx_log_error(NGX_LOG_EMERG, cf->log, 0,
		    "auth_a2aclr_db configuration directive is missing");

		return NGX_CONF_ERROR;
	}

	if (conf->realm == NULL) {
		conf->realm = prev->realm;
	}

	if (conf->class == NULL) {
		conf->class = prev->class;
	}

	if (conf->instance == NULL) {
		conf->instance = prev->instance;
	}

	return NGX_CONF_OK;
}
